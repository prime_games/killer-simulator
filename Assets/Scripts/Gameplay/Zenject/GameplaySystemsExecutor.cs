﻿using System;
using Leopotam.Ecs;
using Zenject;

namespace Gameplay.Zenject
{
    public class GameplaySystemsExecutor : ITickable, IDisposable
    {
        private readonly EcsSystems _systems;

        public GameplaySystemsExecutor(EcsWorld world)
        {
            _systems = new EcsSystems(world, "Gameplay Systems");

            _systems.Init();
        }

        public void Dispose()
        {
            _systems.Destroy();
        }

        public void Tick()
        {
            _systems.Run();
        }
    }
}