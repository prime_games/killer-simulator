﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class StartGame : MonoBehaviour
{
    public BulletLogic BulletLogic;
    
    public Animator playerAnimator;
    public Vector3 RotateAfterShoot;
    private static readonly int IsShooting = Animator.StringToHash("IsShooting");

    public List<Image> bulletList;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            PlayAnimation();
            StartBullet();
            RemoveBulletFromBar();
        }
    }

    private void PlayAnimation()
    {
        playerAnimator.SetBool(IsShooting, true);
        playerAnimator.gameObject.transform.rotation = Quaternion.Euler(RotateAfterShoot);
    }

    private void StartBullet()
    {
        BulletLogic.LaunchBullet();
    }

    private void RemoveBulletFromBar()
    {
        for (int i = 0; i < bulletList.Count; i++)
        {
            if (bulletList[i].gameObject.activeSelf)
            {
                bulletList[i].DOFade(0.5f, 1.0f);
                break;
            }
        }
    }
}