﻿using Base.View;
using Leopotam.Ecs;
using Lobby.Settings.Views;

namespace Lobby.Settings.System
{
    public class SettingsSystem : IEcsInitSystem
    {
        #region Fields
        
        private SettingsView _settingsView;
        private AudioManager _audioManager;
        
        #endregion
        
        #region Constructor
        
        public SettingsSystem(RootView rootView)
        {
            _settingsView = rootView.SettingsView;
        }
        
        #endregion
        
        #region Methods
        
        public void Init()
        {
            _audioManager = _settingsView.AudioManager;
            
            _settingsView.MusicSwitch.onValueChanged.AddListener(OnMusicSwitch);
            _settingsView.SoundsSwitch.onValueChanged.AddListener(OnSoundSwitch);
            _settingsView.VibrationSwitch.onValueChanged.AddListener(OnVibrationSwitch);
        }
        
        
        private void OnSoundSwitch(bool arg0)
        {
            _audioManager.SetSound(!arg0);
        }
        
        private void OnVibrationSwitch(bool arg0)
        {
            GameSettings.VibrationIsActive = arg0;
        }
        
        private void OnMusicSwitch(bool arg0)
        {
            _audioManager.SetMusic(arg0);
        }
        
        
        #endregion
    }
}