﻿using UnityEngine;

namespace Lobby.Settings.System
{
    [AddComponentMenu("Audio/Audio Muter")]
    public class SoundMuter : MonoBehaviour
    {
        private AudioManager audioManager;
        private AudioSource audioSource;
        
        void Start()
        {
            audioSource = gameObject.GetComponent<AudioSource>();
            audioManager = FindObjectOfType<AudioManager>();
        
            audioManager.OnAudioSettingsChanged += OnAudioSettingsChanged;
            OnAudioSettingsChanged();
        }
        private void OnAudioSettingsChanged()
        { 
            audioSource.volume = !GameSettings.SoundIsMuted ? 1F : 0F;
        }
        void OnDestroy()
        {
            audioManager.OnAudioSettingsChanged -= OnAudioSettingsChanged;
        }
    }
}