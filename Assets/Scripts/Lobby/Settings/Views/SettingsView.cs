﻿using UnityEngine;
using UnityEngine.UI;

namespace Lobby.Settings.Views
{
    [DisallowMultipleComponent]
    public class SettingsView : MonoBehaviour
    {
        #region Fields

        [SerializeField] private Toggle musicSwitch;
        [SerializeField] private Toggle soundsSwitch;
        [SerializeField] private Toggle vibrationSwitch;
        [SerializeField] private AudioManager audioManager;

        #endregion
        
        #region Properties

        public AudioManager AudioManager => audioManager;

        public Toggle MusicSwitch => musicSwitch;

        public Toggle SoundsSwitch => soundsSwitch;

        public Toggle VibrationSwitch => vibrationSwitch;

        #endregion
    }
}
