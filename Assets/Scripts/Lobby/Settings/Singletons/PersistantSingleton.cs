﻿using UnityEngine;

namespace PG.Common
{
    /// <summary>
    /// ...
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DisallowMultipleComponent]
    public class PersistantSingleton<T> : BaseSingleton<T> where T : MonoBehaviour
    {
        #region Methods

        protected override void Init()
        {
            base.Init();
            DontDestroyOnLoad(gameObject);
        }

        #endregion
    }
}
