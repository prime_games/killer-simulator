﻿using Lobby.Settings.System;
using Zenject;

namespace Lobby.Zenject
{
    public class LobbyInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<SettingsSystem>().AsSingle();

            Container.BindInterfacesAndSelfTo<LobbySystemsExecutor>().AsSingle().NonLazy();
        }
    }
}