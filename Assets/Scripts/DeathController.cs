﻿using UnityEngine;

public class DeathController : MonoBehaviour
{
    public Animator animator;
    private static readonly int IsDeath = Animator.StringToHash("IsDeath");

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            PlayDeathAnimation();
        }
    }

    public void PlayDeathAnimation()
    {
        animator.SetBool(IsDeath, true);
    }
}