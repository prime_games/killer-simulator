﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BulletLogic : MonoBehaviour
{
    public List<Transform> pathPositions;

    public List<Vector3> rotateAfterPosition;

    public Transform nextPosition;
    public int nextPositionIndex;

    public float distanceDelta = 0.15f;
    public float speed = 1.5f;
    public float rotationDuration = 1f;

    public bool lastPathAchieved;

    public GameObject TargetObject;

    public TrailRenderer Trail;

    public GameObject ShootEffect;
    public GameObject OnTriggerEffect;
    public GameObject RicochetEffect;

    void Start()
    {
        lastPathAchieved = true;
        nextPosition = pathPositions[0];
        gameObject.SetActive(false);
    }

    public void LaunchBullet()
    {
        gameObject.SetActive(true);
        lastPathAchieved = false;
        CreateEffect(ShootEffect, transform.position);
    }

    void FixedUpdate()
    {
        if (!lastPathAchieved)
        {
            var distance = Vector3.Distance(nextPosition.position, transform.position);
            transform.position = Vector3.MoveTowards(transform.position, nextPosition.position, speed * Time.deltaTime);

            if (distance < distanceDelta)
            {
                CreateEffect(RicochetEffect, transform.position);


                transform.DORotate(rotateAfterPosition[nextPositionIndex], rotationDuration);

                nextPositionIndex++;
                if (nextPositionIndex < pathPositions.Count)
                {
                    nextPosition = pathPositions[nextPositionIndex];
                }
                else
                {
                    if (TargetObject != null)
                    {
                        var deathController = TargetObject.GetComponent<DeathController>();
                        deathController.PlayDeathAnimation();
                    }


                    lastPathAchieved = true;
                    CreateEffect(OnTriggerEffect, transform.position);
                    Destroy(gameObject);
                }
            }
        }
    }

    private void CreateEffect(GameObject prefab, Vector3 position)
    {
        var effect = Instantiate(prefab, position, Quaternion.identity);
        Destroy(effect, 2.0f);
    }

    private void OnDestroy()
    {
        Trail.transform.parent = null;
        Trail.autodestruct = true;
    }
}