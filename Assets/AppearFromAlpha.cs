﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class AppearFromAlpha : MonoBehaviour
{
    private Image image;
    public float duration = 2.0f;
    public float startAfter = 2.0f;

    private float startTime;
    private bool isStarted;

    void Start()
    {
        startTime = Time.time + startAfter;
        image = gameObject.GetComponent<Image>();
    }

    void Update()
    {
        if (!isStarted && startAfter < Time.time)
        {
            isStarted = true;
        }

        if (isStarted)
        {
            image.fillAmount = Time.time - startAfter;
        }
    }
}