﻿using UnityEngine;

public class WallCreator : MonoBehaviour
{
    public GameObject StartPosition;
    public GameObject EndPosition;

    public GameObject Prefab;
    public int ObjectCount = 10;

    public float Delta;
    public float TimeDelta;

    private float NextTime;

    private Vector3 CurrentPosition;
    private Vector3 Offset = Vector3.one;

    void Start()
    {
        SetOffset();
        CurrentPosition = StartPosition.transform.position;
    }

    void CreateWall(Vector3 startPosition)
    {
        var wallElement = Instantiate(Prefab, startPosition, Prefab.transform.rotation);
        wallElement.transform.SetParent(gameObject.transform, false);
        if (Vector3.Distance(startPosition, EndPosition.transform.position) <= Delta)
        {
            return;
        }

        CurrentPosition = GetNextPosition(CurrentPosition);
    }

    void Update()
    {
        if (Time.time >= NextTime)
        {
            NextTime = Time.time + TimeDelta;
            CreateWall(CurrentPosition);
        }
    }

    private Vector3 GetNextPosition(Vector3 currentPosition)
    {
        return currentPosition - Offset;
    }

    private void SetOffset()
    {
        Offset = (StartPosition.transform.position - EndPosition.transform.position) / ObjectCount;
    }
}